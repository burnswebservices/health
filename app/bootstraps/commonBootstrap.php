<?php

// Global Constants
define('APPLICATION_PATH', realpath(dirname(__DIR__) . '/../app'));
define('APPLICATION_START_TIME', microtime(true));

// PHP Settings
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('date.timezone', 'America/Los_Angeles');
ini_set('error_log', APPLICATION_PATH . '/logs/php-error_' . date('Ymd') . '.log');

// Error handler
//require_once(APPLICATION_PATH . '/libraries/systemErrorHandler.php');

// 3rd Party Libraries
// require_once(APPLICATION_PATH . '/../vendor/autoload.php');

// Config
$config = include APPLICATION_PATH . "/config/config.php";

// Cache
$frontCache = new \Phalcon\Cache\Frontend\Data(array(
    "lifetime" => 172800
));
$cache = new Phalcon\Cache\Backend\Redis($frontCache, array(
    'host' => 'localhost',
    'port' => 6379,
    'persistent' => false
));

// Throw Exception on Failed Save
\Phalcon\Mvc\Model::setup(['exceptionOnFailedSave' => true]);