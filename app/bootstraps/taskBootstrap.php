<?php

/**
 * Bootstrap File for tasks ran from command line.
 *
 * Sample: php -f run-task.php <task> <action>
 * Sample: php -f run-task.php scheduler run
 */
define('APPLICATION_INTERFACE', 'TASK');
define('CURRENT_TASK', (isset($argv[1]) ? $argv[1] : null));
define('CURRENT_ACTION', (isset($argv[2]) ? $argv[2] : null));

try {
    require_once('commonBootstrap.php');

    // Phalcon AutoLoader
    $loader = new \Phalcon\Loader();
    $loader->registerDirs(
        array(
            APPLICATION_PATH . '/tasks/',
            APPLICATION_PATH . '/models/',
            APPLICATION_PATH . '/libraries/'
        )
    )->register();

    /**
     * Setup Dependency Injection
     *
     * @link http://docs.phalconphp.com/en/latest/api/Phalcon_DI.html
     */
    // Using the CLI factory default services container
    $di = new \Phalcon\DI\FactoryDefault\CLI();

    $di->setShared('cache', $cache);

    // Registering a dispatcher
    $di->setShared(
        'dispatcher', function () use ($di) {
        $dispatcher = new \Phalcon\CLI\Dispatcher;
        $dispatcher->setDI($di);
        return $dispatcher;
    }
    );

    // Register Session
    $session = new \Phalcon\Session\Adapter\Files();
    $session->start();
    $di->setShared('session', $session);

    // Register Config Files
    $di->setShared('config', $config);

    // Database connection is created based in the parameters defined in the configuration file
    $di->set(
        'db', function () use ($config) {
        return new \Phalcon\Db\Adapter\Pdo\Mysql(
            array(
                'host'     => $config->database->host,
                'username' => $config->database->username,
                'password' => $config->database->password,
                'dbname'   => $config->database->dbname
            )
        );
    }
    );

    /**
     * Handle the request
     */
    // Process the console arguments
    $arguments = array();
    foreach ($argv as $k => $arg) {
        if ($k == 1) {
            $arguments['task'] = $arg;
        } elseif ($k == 2) {
            $arguments['action'] = $arg;
        } elseif ($k >= 3) {
            $arguments['params'][] = $arg;
        }
    }

    $console = new \Phalcon\CLI\Console();
    $console->setDI($di);

    $console->handle($arguments);

} catch (\Exception $e) {
    echo "Bootstrap Exception: ", $e->getMessage() . $e->getTraceAsString();
}
