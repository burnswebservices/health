<?php
return new \Phalcon\Config(array(
    'database' => array(
        'adapter' => 'Mysql',
        'host' => 'localhost',
        'username' => 'root',
        'password' => 'dc74uc',
        'dbname' => 'health',
    ),
    'application' => array(
        'modelsDir' => 'app/models/',
        'controllersDir' => 'app/controllers/',
        'viewsDir' => 'app/views/',
        'pluginsDir' => 'app/plugins/',
        'libraryDir' => 'app/library/',
        'baseUri' => '/'
    )
));