<?php

class IndexController extends ControllerBase
{
    
    /**
     * indexAction
     */
    public function indexAction()
    {
        $this->view->monitorItems = MonitorItems::find();
    }
    
}

