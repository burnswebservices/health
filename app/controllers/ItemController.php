<?php

class ItemController extends \Phalcon\Mvc\Controller
{

    public function addAction()
    {
        if($this->request->isPost()) {
            $monitorData = new MonitorData();
            // @TODO move to model
            $_POST['created_at'] = date('Y-m-d H:i:s');
            $monitorData->save($_POST);
        }
        $this->response->redirect('/');
    }

}

