<?php

class MonitorData extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $monitor_item_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $value;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('monitor_item_id', 'MonitorItems', 'id', ['alias' => 'MonitorItems']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'monitor_data';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MonitorData[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MonitorData
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
